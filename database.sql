-- Active: 1622622425311@@127.0.0.1@3306@p23_revision
DROP TABLE IF EXISTS bike_part;
DROP TABLE IF EXISTS bike;
DROP TABLE IF EXISTS part;

DROP TABLE IF EXISTS brand;


CREATE TABLE brand(
    id INT PRIMARY KEY AUTO_INCREMENT,
    label VARCHAR(255) NOT NULL
);

CREATE TABLE bike(
    id INT PRIMARY KEY AUTO_INCREMENT,
    model VARCHAR(255) NOT NULL,
    size VARCHAR(5),
    picture VARCHAR(255),
    id_brand INT,
    Foreign Key (id_brand) REFERENCES brand(id)
);

CREATE TABLE part (
    
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL
);

CREATE TABLE bike_part (
    id_bike INT,
    id_part INT,
    PRIMARY KEY (id_part, id_bike),
    Foreign Key (id_part) REFERENCES part(id),
    Foreign Key (id_bike) REFERENCES bike(id)
);

INSERT INTO part (name) VALUES ('Dérailleur'), ('Brakes'), ('Brakes patins');

INSERT INTO brand (label) VALUES ('Giant'), ('decathlon'), ('trek');

INSERT INTO bike (model, id_brand, size, picture) VALUES ('Riverside', 2, 'L', 'https://cdn.pixabay.com/photo/2013/07/13/13/43/racing-bicycle-161449_1280.png'),
('ZXC3', 1, 'M', 'https://cdn.pixabay.com/photo/2015/05/29/19/18/bicycle-789648_1280.jpg'),
('Model 2023', 3, 'XS', 'https://cdn.pixabay.com/photo/2017/06/27/11/52/mountain-bike-2447170_1280.jpg');

INSERT INTO bike_part (id_bike,id_part) VALUES (1,1), (1,2), (1,3), (2, 2);