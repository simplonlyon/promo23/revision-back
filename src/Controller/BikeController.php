<?php

namespace App\Controller;

use App\Entity\Bike;
use App\Repository\BikeRepository;
use App\Service\Uploader;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Serializer\SerializerInterface;

#[Route('/api/bike')]
class BikeController extends AbstractController
{
    public function __construct(private BikeRepository $repo) {}

    #[Route(methods:'GET')]
    public function all(): JsonResponse
    {
        return $this->json($this->repo->findAll());
    }

    #[Route(methods:'POST')]
    public function add(Request $request, SerializerInterface $serializer, ValidatorInterface $validator, Uploader $uploader): JsonResponse
    {
        try{
            $bike = $serializer->deserialize($request->getContent(), Bike::class, 'json');
        }catch(\Exception $e) {
            return $this->json('Invalid body', 400);
        }
        $errors = $validator->validate($bike);
        if($errors->count() > 0) {
            return $this->json($errors, 400);
        }

        $bike->setPicture( $uploader->upload($bike->getPicture()) );
        $this->repo->persist($bike);
        return $this->json($bike, 201);
    }
}
