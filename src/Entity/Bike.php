<?php

namespace App\Entity;
use Symfony\Component\Validator\Constraints as Assert;

class Bike {
    private ?int $id;
    #[Assert\NotBlank]
    private string $model;
    private ?string $brand;
    #[Assert\NotBlank]
    private string $size;

	private string $picture;

	/**
	 * @return int|null
	 */
	public function getId(): ?int {
		return $this->id;
	}
	
	/**
	 * @param int|null $id 
	 * @return self
	 */
	public function setId(?int $id): self {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getModel(): string {
		return $this->model;
	}
	
	/**
	 * @param string $model 
	 * @return self
	 */
	public function setModel(string $model): self {
		$this->model = $model;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getBrand(): ?string {
		return $this->brand;
	}
	
	/**
	 * @param string $brand 
	 * @return self
	 */
	public function setBrand(?string $brand): self {
		$this->brand = $brand;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getSize(): string {
		return $this->size;
	}
	
	/**
	 * @param string $size 
	 * @return self
	 */
	public function setSize(string $size): self {
		$this->size = $size;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getPicture(): string {
		return $this->picture;
	}
	
	/**
	 * @param string $picture 
	 * @return self
	 */
	public function setPicture(string $picture): self {
		$this->picture = $picture;
		return $this;
	}
}