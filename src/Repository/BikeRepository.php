<?php

namespace App\Repository;
use App\Entity\Bike;


class BikeRepository {

    /**
     * @return Bike[]
     */
    public function findAll():array {
        $list = [];
        $connection = Connection::getConnection();
        $query = $connection->prepare('SELECT bike.*, brand.label AS brand FROM bike LEFT JOIN brand ON bike.id_brand=brand.id');
        $query->execute();
        foreach($query->fetchAll() as $line) {
            $bike = new Bike();
            $bike->setModel($line['model']);
            $bike->setBrand($line['brand']);
            $bike->setSize($line['size']);
            $bike->setPicture($line['picture']);
            $bike->setId($line['id']);
            $list[] = $bike;
        }

        return $list;
    }

    public function persist(Bike $bike):void {
        
        $connection = Connection::getConnection();
        $query = $connection->prepare('INSERT INTO bike (model, size, picture) VALUES (:model,:size,:picture)');
        $query->bindValue(':model', $bike->getModel());
        $query->bindValue(':size', $bike->getSize());
        $query->bindValue(':picture', $bike->getPicture());
        $query->execute();
        
        $bike->setId($connection->lastInsertId());

    }
}