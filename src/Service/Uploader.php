<?php

namespace App\Service;

class Uploader {

    public function upload(string $base64):string {
        $filename = uniqid() . '.jpg';

        if (!file_exists('uploads')) {
            mkdir('uploads', 0777, true);
        }

        [$prefix, $img64] = explode( ',', $base64);
        $decoded = base64_decode($img64);
        $image = imagecreatefromstring($decoded);
        $image = imagescale($image, 1024);
        imagejpeg($image, 'uploads/'.$filename);
        imagedestroy($image);

        return $filename;
    }
}